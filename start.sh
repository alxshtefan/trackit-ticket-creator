case "$OSTYPE" in
  darwin*)  open index.html ;;
  linux*)   xdg-open index.html ;;
  msys*)    start  index.html ;;
  cygwin*)  start  index.html ;;
  *)        echo "unknown: $OSTYPE" ;;
esac
