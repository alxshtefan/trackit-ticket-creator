/**
 *
 * If you need to add one more vehicle number, please follow format below.
 *
 *  <name_to_display> : <number_in_the_db>
 *
 */

const VEHICLE_NUMBERS = {
  "rogOneVehicle1": "7111",
  "rogtwoVehicle1": "7777D",
  "vasiVeh": "88002",
  "oleksVehicleRogue2": "OS1818"
};