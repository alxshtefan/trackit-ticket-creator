/**
 *
 * JSON data should contains placeholders for the js injection.
 *  #PH_DATE_NOW# : date of the creation
 *  #PH_TICKET_TYPE# : type of the ticket
 *  #PH_EID# : environment id
 *  #PH_TICKET_NUMBER# : ticket number
 *  #PH_VEHICLE_NUMBER# : vehicle number
 *
 */

const DATE_NOW_PLACEHOLDER = "#PH_DATE_NOW#";
const ENVIRONMENT_PLACEHOLDER = "#PH_EID#";
const TICKET_NUMBER_PLACEHOLDER = "#PH_TICKET_NUMBER#";
const VEHICLE_NUMBER_PLACEHOLDER = "#PH_VEHICLE_NUMBER#";

const CREATE = {
  "DLV": {
    "Item": {
      "data": {
        "customData": {
          "statusCode": "TO_JOB",
          "ticketType": "DLV",
          "runSheetNumber": "2345234532452345234566",
          "manualLoadTrigger": false,
          "schedule": {
            "pickupTimeFrom": "#PH_DATE_NOW#",
            "pickupTimeTo": "#PH_DATE_NOW#",
            "deliveryTimeFrom": "#PH_DATE_NOW#",
            "deliveryTimeTo": "#PH_DATE_NOW#",
            "agreeWindowFrom": "#PH_DATE_NOW#",
            "agreeWindowTo": "#PH_DATE_NOW#"
          }
        },
        "connex": {
          "mod": {
            "type": "C",
            "modifyDate": "#PH_DATE_NOW#"
          }
        },
        "pk": "connex-#PH_EID#-ticket",
        "crn": "22461204-c68e-5331-80dc-90f6c3765e84",
        "typeId": "ticket",
        "CONNEX": {
          "EID": "#PH_EID#",
          "SID": "ticket"
        },
        "primaryKey": "#PH_DATE_NOW#",
        "lookupKey": "#PH_TICKET_NUMBER#",
        "active": true,
        "purchaseOrder": null,
        "dataModelVersion": "2.0.99",
        "originatorSystemType": "HC",
        "originatorSystemId": "#PH_TICKET_NUMBER#",
        "originatorClientId": "#PH_EID#",
        "originatorCreatedDateTime": "#PH_DATE_NOW#",
        "originatorModifiedDateTime": "#PH_DATE_NOW#",
        "originatorRecordId": "22461204-c68e-5331-80dc-90f6c3765e84",
        "createdBy": {
          "id": "HC",
          "name": "HC"
        },
        "id": "#PH_TICKET_NUMBER#",
        "isVoided": false,
        "loadNumber": null,
        "dispatchDateTime": "#PH_DATE_NOW#",
        "currencyCode": null,
        "paymentMethod": null,
        "supplierParty": {
          "originatorRecordId": "#PH_EID#"
        },
        "customerParty": {
          "originatorRecordId": null,
          "contacts": [],
          "address": null,
          "job": null
        },
        "carrierParty": {
          "entityRef": null,
          "id": null,
          "name": null,
          "originatorRecordId": null,
          "contacts": []
        },
        "driver": {
          "id": null,
          "originatorRecordId": null,
          "userRef": null,
          "firstName": null,
          "lastName": null
        },
        "salesPerson": {
          "originatorRecordId": null,
          "userRef": null
        },
        "weighMaster": {
          "id": null,
          "originatorRecordId": null,
          "firstName": null,
          "lastName": null
        },
        "containers": [
          {
            "vehicle": {
              "id": "#PH_VEHICLE_NUMBER#",
              "description": "#PH_VEHICLE_NUMBER#",
              "vehicleType": {
                "id": null,
                "description": null
              },
              "originatorRecordId": null,
              "licensePlate": null
            },
            "item": {
              "id": "1"
            },
            "maxGrossWeight": {
              "value": null,
              "uomCode": null
            },
            "grossWeight": {
              "value": 0,
              "uomCode": "TO"
            },
            "netWeight": {
              "value": 30,
              "uomCode": "TO"
            },
            "tareWeight": {
              "value": 0,
              "uomCode": "TO"
            }
          }
        ],
        "jobArea": {
          "id": null,
          "description": null,
          "jobAreaRef": null
        },
        "dispatchOrder": {
          "dispatchOrderRef": null,
          "id": null,
          "originatorRecordId": null,
          "description": null,
          "totalOrdered": null,
          "totalTicketed": {
            "reasonCode": null,
            "loadNumber": null,
            "value": null,
            "uomCode": null
          }
        },
        "order": {
          "orderRef": null,
          "id": null
        },
        "orderRequest": {
          "orderRequestRef": null,
          "id": null,
          "description": null
        },
        "orderDiscipline": {
          "disciplineRef": null,
          "id": null,
          "description": null
        },
        "invoiceFrequency": null,
        "commerceTypeCode": "SELL",
        "salesAnalysis": {
          "id": null,
          "description": null
        },
        "deliveryOption": null,
        "transportationTermsCode": null,
        "inventoryLocation": null,
        "supplierSalesOrder": {
          "id": null,
          "originatorRecordId": null,
          "name": null
        },
        "origin": {
          "originatorRecordId": "GB07",
          "id": "GB07",
          "description": "Middlesbrough Depot",
          "latitude": 54.580914,
          "longitude": -1.247161,
          "address": {
            "address1": "Forty Foot Road",
            "city": "Middlesbrough",
            "postalCode": "TS2 1HG GB",
            "countryCode": "GBR",
            "contact": {}
          },
          "scale": {
            "id": null,
            "name": null
          }
        },
        "invoice": {
          "id": null,
          "invoiceDateTime": null
        },
        "destination": {
          "deliveryZone": {
            "id": null,
            "description": null
          },
          "deliveryBlock": null,
          "deliveryLots": [],
          "id": "Segment Factory Mob",
          "originatorRecordId": "Segment Factory Mob",
          "description": "Segment Factory {Mobile Baustoffe} Strabag AG UK Branch",
          "address": {
            "address1": "515 Greenbrier Way",
            "city": "Hoover",
            "postalCode": "35244",
            "countryCode": "US",
            "countrySubDivision": "AL",
            "contact": {
              "name": "Gill Simpson",
              "phone": "07976 740389"
            }
          },
          "instructions": "NEAREST CROSS RD: Wilton Site"
        },
        "primaryItemType": "CONCRETE",
        "lineItems": [
          {
            "customerOrderItem": {
              "haulCostCode": null,
              "productCostCode": null,
              "purchaseOrder": {
                "number": null,
                "line": null
              },
              "typeOfWork": null
            },
            "item": {
              "productRef": null,
              "id": "1",
              "description": "1",
              "originatorRecordId": "1",
              "itemType": "CONCRETE",
              "itemSubType": null,
              "pricingPlanId": null,
              "itemCategoryId": null
            },
            "customerItem": {
              "id": null,
              "description": null
            },
            "quantity": {
              "value": 30,
              "uomCode": "TO"
            },
            "soldQuantity": {
              "value": null,
              "uomCode": null
            },
            "unitPrice": 0,
            "extendedPrice": 0,
            "orderedQuantity": null,
            "batchQuantity": {
              "value": null,
              "uomCode": null
            },
            "returnedQuantity": {
              "value": null,
              "uomCode": null
            },
            "dumpedQuantity": {
              "value": null,
              "uomCode": null
            },
            "reshippedTicket": {
              "id": null,
              "originatorRecordId": null,
              "originatorSystemType": null,
              "originatorSystemId": null,
              "reshippedQuantity": {
                "value": null,
                "uomCode": null
              }
            },
            "isPrimary": true
          }
        ],
        "priceSummary": {
          "taxRate": null,
          "pricingLocation": null,
          "subtotalPrice": 0,
          "taxRegion": null,
          "taxCode": null,
          "taxAmount": 0,
          "totalPrice": 0,
          "totalFreightCharge": 0,
          "subTotalPrice": 0
        },
        "eTicketStatus": null,
        "customerStatus": {
          "statusCode": "PENDING"
        },
        "eTicketContacts": [],
        "supplierStatus": {
          "reasonCode": null,
          "statusCode": "CREATED"
        },
        "displayLabel": "#PH_TICKET_NUMBER#",
        "sk": {
          "dispatchDay": "27",
          "dispatchMonth": "06",
          "dispatchYear": "2022",
          "dispatchHour": "19",
          "carrierRef": null,
          "id": "#PH_TICKET_NUMBER#",
          "sourceTenantRef": "#PH_EID#",
          "uom": "TO",
          "lookupKey": "#PH_TICKET_NUMBER#",
          "tenantRef": "#PH_EID#",
          "carrierId": null,
          "carrierName": null,
          "vehicleId": "#PH_VEHICLE_NUMBER#",
          "vehicleDescription": "#PH_VEHICLE_NUMBER#",
          "customerJob": null,
          "productId": "1",
          "disciplineId": null,
          "customerName": null,
          "orderTicketRef": null,
          "customerRef": null,
          "testResultStatus": null,
          "supplierStatus": "CREATED",
          "projectName": null,
          "projectId": null,
          "haulUnitCost": 0,
          "supplierId": null,
          "dispatchOrderDescription": null,
          "dispatchDate": "#PH_DATE_NOW#",
          "productUnitCost": 0,
          "sourceSystem": "HC",
          "dispatchOrderId": null,
          "jobAreaId": null,
          "jobAreaDescription": null,
          "customerStatus": "PENDING",
          "originId": "GB07",
          "customerId": null,
          "supplierVoided": false,
          "crn": "22461204-c68e-5331-80dc-90f6c3765e84",
          "productType": "CONCRETE",
          "latestEventStatus": "CREATED",
          "ticketEventStatus": "CREATED",
          "ticketEventStatusTime": "#PH_DATE_NOW#",
          "supplierName": null,
          "quantity": 30,
          "ticketType": "production",
          "disciplineDescription": null,
          "supplierRef": "#PH_EID#",
          "purchaseOrder": null,
          "invoiceId": null,
          "originDescription": "Middlesbrough Depot"
        }
      }
    }
  },
  "HOP": {
    "Item": {
      "data": {
        "customData":{
          "ticketType": "HOP",
          "runSheetNumber": "2345234532452345234566",
          "schedule":{
            "pickupTimeFrom": "#PH_DATE_NOW#",
            "pickupTimeTo": "#PH_DATE_NOW#",
          }
        },
        "connex": {
          "dataModelVersion": "2.0",
          "mod": {
            "type": "C",
            "modifyDate": "#PH_DATE_NOW#"
          }
        },
        "pk": "connex-#PH_EID#-ticket",
        "crn": "5f14c55d-fa4f-5437-8919-765e2ee0aad0",
        "typeId": "ticket",
        "CONNEX": {
          "EID": "#PH_EID#",
          "SID": "ticket"
        },
        "primaryKey": "#PH_DATE_NOW#",
        "lookupKey": "#PH_TICKET_NUMBER#",
        "active": true,
        "purchaseOrder": "TESTORDER",
        "dataModelVersion": "2.0.99",
        "originatorSystemType": "CONNEX",
        "originatorClientId": "#PH_EID#",
        "originatorCreatedDateTime": "#PH_DATE_NOW#",
        "originatorModifiedDateTime": "#PH_DATE_NOW#",
        "originatorRecordId": "5f14c55d-fa4f-5437-8919-765e2ee0aad0",
        "createdBy": {
          "id": "secret@commandalkon.com",
          "name": "Did you read"
        },
        "id": "#PH_TICKET_NUMBER#",
        "isVoided": false,
        "loadNumber": 2,
        "dispatchDateTime": "#PH_DATE_NOW#",
        "currencyCode": "USD",
        "paymentMethod": null,
        "supplierParty": {
          "name": "Test Batch Plant LLC         ",
          "entityRef": "#PH_EID#",
          "costBookRef": "3fbded5a-3fdf-431a-a8db-218224d151b7",
          "originatorRecordId": "#PH_EID#"
        },
        "customerParty": {
          "name": "CONNEX Test Customer",
          "entityRef": "389d21a3-9a53-40f3-a842-6f9eb27bfef4",
          "id": "TEST",
          "originatorRecordId": "389d21a3-9a53-40f3-a842-6f9eb27bfef4",
          "contacts": [],
          "address": {
            "coordinates": {
              "longitude": -86.76286,
              "latitude": 26.40531
            },
            "countrySubDivision": "AL",
            "city": "Birmingham",
            "address1": "1800 International Park Drive",
            "postalCode": "35243"
          },
          "job": null
        },
        "carrierParty": {
          "entityRef": "58ea7159-8984-58f3-bb41-57f2b4d2ad7f",
          "id": "00000000",
          "name": "MASTER VENDOR",
          "originatorRecordId": "58ea7159-8984-58f3-bb41-57f2b4d2ad7f",
          "contacts": []
        },
        "destination": {
          "address": {
            "coordinates": {
              "longitude": -86.76286,
              "latitude": 26.40531
            },
            "countrySubDivision": "AL",
            "city": "Birmingham",
            "address1": "1800 International Park Drive",
            "postalCode": "35243"
          },
          "deliveryZone": {
            "id": null,
            "description": null
          },
          "deliveryBlock": null,
          "deliveryLots": [],
          "id": null,
          "originatorRecordId": null,
          "description": null,
          "latitude": null,
          "longitude": null,
          "contacts": [
            {
              "contactRole": "general",
              "userRef": null,
              "name": null,
              "phone": null,
              "email": null
            }
          ],
          "instructions": null
        },
        "sk": {
          "dispatchDay": "19",
          "dispatchMonth": "10",
          "dispatchYear": "2021",
          "dispatchHour": "19",
          "carrierRef": "58ea7159-8984-58f3-bb41-57f2b4d2ad7f",
          "id": "#PH_TICKET_NUMBER#",
          "sourceTenantRef": "#PH_EID#",
          "uom": "YDQ",
          "lookupKey": "#PH_TICKET_NUMBER#",
          "tenantRef": "#PH_EID#",
          "productDescription": "Test Product",
          "carrierId": "00000000",
          "carrierName": "MASTER VENDOR",
          "vehicleId": "#PH_VEHICLE_NUMBER#",
          "vehicleDescription": "TEST TRUCK",
          "customerJob": null,
          "productId": "TESTPROD",
          "disciplineId": null,
          "customerName": "CONNEX Test Customer",
          "orderTicketRef": "2fd7cd34-de4b-4f1d-9847-fc024543b761",
          "customerRef": "389d21a3-9a53-40f3-a842-6f9eb27bfef4",
          "testResultStatus": null,
          "supplierStatus": "CREATED",
          "projectName": "None Selected",
          "projectId": "NONE",
          "haulUnitCost": 0,
          "supplierId": null,
          "dispatchOrderDescription": null,
          "dispatchDate": "#PH_DATE_NOW#",
          "productUnitCost": 0,
          "sourceSystem": "CONNEX",
          "dispatchOrderId": "50004",
          "jobAreaId": null,
          "jobAreaDescription": null,
          "customerStatus": "PENDING",
          "originId": "TestLOC",
          "customerId": "TEST",
          "supplierVoided": false,
          "crn": "5f14c55d-fa4f-5437-8919-765e2ee0aad0",
          "productType": "CONCRETE",
          "latestEventStatus": "CREATED",
          "ticketEventStatus": "CREATED",
          "ticketEventStatusTime": "#PH_DATE_NOW#",
          "supplierName": "JCK Batch Plant LLC         ",
          "quantity": 100,
          "ticketType": "production",
          "disciplineDescription": null,
          "supplierRef": "#PH_EID#",
          "purchaseOrder": "TESTORDER",
          "invoiceId": null,
          "originDescription": "Test Plant"
        },
        "ticketEvents": {
          "CREATED": {
            "eventDateTime": "#PH_DATE_NOW#"
          }
        },

        "createDate": "#PH_DATE_NOW#",
        "modifyDate": "2021-10-19T19:08:43Z",
        "pko": "connex-#PH_EID#-ticket"
      }
    }
  },
  "HOM": {
    "Item": {
      "data": {
        "customData": {
          "ticketType": "HOM",
          "runSheetNumber": "2345234532452345234566"
        },
        "connex": {
          "mod": {
            "type": "C",
            "modifyDate": "#PH_DATE_NOW#"
          }
        },
        "pk": "connex-#PH_EID#-ticket",
        "crn": "22461204-c68e-5331-80dc-90f6c3765e84",
        "typeId": "ticket",
        "CONNEX": {
          "EID": "#PH_EID#",
          "SID": "ticket"
        },
        "primaryKey": "#PH_DATE_NOW#",
        "lookupKey": "#PH_TICKET_NUMBER#",
        "active": true,
        "purchaseOrder": null,
        "dataModelVersion": "2.0.99",
        "originatorSystemType": "HC",
        "originatorSystemId": "#PH_TICKET_NUMBER#",
        "originatorClientId": "#PH_EID#",
        "originatorCreatedDateTime": "#PH_DATE_NOW#",
        "originatorModifiedDateTime": "#PH_DATE_NOW#",
        "originatorRecordId": "22461204-c68e-5331-80dc-90f6c3765e84",
        "createdBy": {
          "id": "HC",
          "name": "HC"
        },
        "id": "#PH_TICKET_NUMBER#",
        "isVoided": false,
        "loadNumber": null,
        "dispatchDateTime": "#PH_DATE_NOW#",
        "currencyCode": null,
        "paymentMethod": null,
        "supplierParty": {
          "originatorRecordId": "#PH_EID#"
        },
        "customerParty": {
          "originatorRecordId": null,
          "contacts": [],
          "address": null,
          "job": null
        },
        "carrierParty": {
          "entityRef": null,
          "id": null,
          "name": null,
          "originatorRecordId": null,
          "contacts": []
        },
        "driver": {
          "id": null,
          "originatorRecordId": null,
          "userRef": null,
          "firstName": null,
          "lastName": null
        },
        "salesPerson": {
          "originatorRecordId": null,
          "userRef": null
        },
        "weighMaster": {
          "id": null,
          "originatorRecordId": null,
          "firstName": null,
          "lastName": null
        },
        "containers": [
          {
            "vehicle": {
              "id": "#PH_VEHICLE_NUMBER#",
              "description": "#PH_VEHICLE_NUMBER#",
              "vehicleType": {
                "id": null,
                "description": null
              },
              "originatorRecordId": null,
              "licensePlate": null
            },
            "item": {},
            "maxGrossWeight": {
              "value": null,
              "uomCode": null
            },
            "grossWeight": {},
            "netWeight": {},
            "tareWeight": {}
          }
        ],
        "jobArea": {
          "id": null,
          "description": null,
          "jobAreaRef": null
        },
        "dispatchOrder": {
          "dispatchOrderRef": null,
          "id": null,
          "originatorRecordId": null,
          "description": null,
          "totalOrdered": null,
          "totalTicketed": {
            "reasonCode": null,
            "loadNumber": null,
            "value": null,
            "uomCode": null
          }
        },
        "order": {
          "orderRef": null,
          "id": null
        },
        "orderRequest": {
          "orderRequestRef": null,
          "id": null,
          "description": null
        },
        "orderDiscipline": {
          "disciplineRef": null,
          "id": null,
          "description": null
        },
        "invoiceFrequency": null,
        "commerceTypeCode": "SELL",
        "salesAnalysis": {
          "id": null,
          "description": null
        },
        "deliveryOption": null,
        "transportationTermsCode": null,
        "inventoryLocation": null,
        "supplierSalesOrder": {
          "id": null,
          "originatorRecordId": null,
          "name": null
        },
        "origin": {
          "originatorRecordId": "BES4",
          "id": "BES4",
          "description": "IB Brussel",
          "latitude": 50.866695,
          "longitude": 4.35333,
          "address": {
            "address1": "Havenlaan 63 Avenue du port",
            "city": "Brussel",
            "postalCode": "1000 BE",
            "countryCode": "BEN",
            "contact": {}
          },
          "scale": {
            "id": null,
            "name": null
          }
        },
        "invoice": {
          "id": null,
          "invoiceDateTime": null
        },
        "destination": {
          "deliveryZone": {
            "id": null,
            "description": null
          },
          "deliveryBlock": null,
          "deliveryLots": [],
          "description": "undefined undefined",
          "address": {
            "contact": {}
          }
        },
        "primaryItemType": "CONCRETE",
        "lineItems": [
          {
            "customerOrderItem": {
              "haulCostCode": null,
              "productCostCode": null,
              "purchaseOrder": {
                "number": null,
                "line": null
              },
              "typeOfWork": null
            },
            "item": {
              "productRef": null,
              "itemType": "CONCRETE",
              "itemSubType": null,
              "pricingPlanId": null,
              "itemCategoryId": null
            },
            "customerItem": {
              "id": null,
              "description": null
            },
            "quantity": {
              "uomCode": "M3"
            },
            "soldQuantity": {
              "value": null,
              "uomCode": null
            },
            "unitPrice": 0,
            "extendedPrice": 0,
            "orderedQuantity": null,
            "batchQuantity": {
              "value": null,
              "uomCode": null
            },
            "returnedQuantity": {
              "value": null,
              "uomCode": null
            },
            "dumpedQuantity": {
              "value": null,
              "uomCode": null
            },
            "reshippedTicket": {
              "id": null,
              "originatorRecordId": null,
              "originatorSystemType": null,
              "originatorSystemId": null,
              "reshippedQuantity": {
                "value": null,
                "uomCode": null
              }
            },
            "isPrimary": true
          }
        ],
        "priceSummary": {
          "taxRate": null,
          "pricingLocation": null,
          "subtotalPrice": 0,
          "taxRegion": null,
          "taxCode": null,
          "taxAmount": 0,
          "totalPrice": 0,
          "totalFreightCharge": 0,
          "subTotalPrice": 0
        },
        "eTicketStatus": null,
        "customerStatus": {
          "statusCode": "PENDING"
        },
        "eTicketContacts": [],
        "supplierStatus": {
          "reasonCode": null,
          "statusCode": "CREATED"
        },
        "displayLabel": "#PH_TICKET_NUMBER#",
        "sk": {
          "dispatchDay": "27",
          "dispatchMonth": "06",
          "dispatchYear": "2022",
          "dispatchHour": "19",
          "carrierRef": null,
          "id": "#PH_TICKET_NUMBER#",
          "sourceTenantRef": "#PH_EID#",
          "uom": "M3",
          "lookupKey": "#PH_TICKET_NUMBER#",
          "tenantRef": "#PH_EID#",
          "carrierId": null,
          "carrierName": null,
          "vehicleId": "#PH_VEHICLE_NUMBER#",
          "vehicleDescription": "#PH_VEHICLE_NUMBER#",
          "customerJob": null,
          "disciplineId": null,
          "customerName": null,
          "orderTicketRef": null,
          "customerRef": null,
          "testResultStatus": null,
          "supplierStatus": "CREATED",
          "projectName": null,
          "projectId": null,
          "haulUnitCost": 0,
          "supplierId": null,
          "dispatchOrderDescription": null,
          "dispatchDate": "#PH_DATE_NOW#",
          "productUnitCost": 0,
          "sourceSystem": "HC",
          "dispatchOrderId": null,
          "jobAreaId": null,
          "jobAreaDescription": null,
          "customerStatus": "PENDING",
          "originId": "BES4",
          "customerId": null,
          "supplierVoided": false,
          "crn": "22461204-c68e-5331-80dc-90f6c3765e84",
          "productType": "CONCRETE",
          "latestEventStatus": "CREATED",
          "ticketEventStatus": "CREATED",
          "ticketEventStatusTime": "#PH_DATE_NOW#",
          "supplierName": null,
          "quantity": null,
          "ticketType": "production",
          "disciplineDescription": null,
          "supplierRef": "#PH_EID#",
          "purchaseOrder": null,
          "invoiceId": null,
          "originDescription": "IB Brussel"
        }
      }
    }
  },
  "RET": {
    "Item": {
      "data": {
        "customData": {
          "ticketType": "RET",
          "runSheetNumber": "2345234532452345234566",
          "schedule": {
            "deliveryTimeFrom": "#PH_DATE_NOW#",
            "deliveryTimeTo": "#PH_DATE_NOW#",
          }
        },
        "connex": {
          "mod": {
            "type": "C",
            "modifyDate": "#PH_DATE_NOW#",
          }
        },
        "pk": "connex-#PH_EID#-ticket",
        "crn": "22461204-c68e-5331-80dc-90f6c3765e84",
        "typeId": "ticket",
        "CONNEX": {
          "EID": "#PH_EID#",
          "SID": "ticket"
        },
        "primaryKey": "#PH_DATE_NOW#",
        "lookupKey": "#PH_TICKET_NUMBER#",
        "active": true,
        "purchaseOrder": null,
        "dataModelVersion": "2.0.99",
        "originatorSystemType": "HC",
        "originatorSystemId": "#PH_TICKET_NUMBER#",
        "originatorClientId": "#PH_EID#",
        "originatorCreatedDateTime": "#PH_DATE_NOW#",
        "originatorModifiedDateTime": "#PH_DATE_NOW#",
        "originatorRecordId": "22461204-c68e-5331-80dc-90f6c3765e84",
        "createdBy": {
          "id": "HC",
          "name": "HC"
        },
        "id": "#PH_TICKET_NUMBER#",
        "isVoided": false,
        "loadNumber": null,
        "dispatchDateTime": "#PH_DATE_NOW#",
        "currencyCode": null,
        "paymentMethod": null,
        "supplierParty": {
          "originatorRecordId": "#PH_EID#"
        },
        "customerParty": {
          "originatorRecordId": null,
          "contacts": [],
          "address": null,
          "job": null
        },
        "carrierParty": {
          "entityRef": null,
          "id": null,
          "name": null,
          "originatorRecordId": null,
          "contacts": []
        },
        "driver": {
          "id": null,
          "originatorRecordId": null,
          "userRef": null,
          "firstName": null,
          "lastName": null
        },
        "salesPerson": {
          "originatorRecordId": null,
          "userRef": null
        },
        "weighMaster": {
          "id": null,
          "originatorRecordId": null,
          "firstName": null,
          "lastName": null
        },
        "containers": [
          {
            "vehicle": {
              "id": "#PH_VEHICLE_NUMBER#",
              "description": "#PH_VEHICLE_NUMBER#",
              "vehicleType": {
                "id": null,
                "description": null
              },
              "originatorRecordId": null,
              "licensePlate": null
            },
            "item": {},
            "maxGrossWeight": {
              "value": null,
              "uomCode": null
            },
            "grossWeight": {},
            "netWeight": {},
            "tareWeight": {}
          }
        ],
        "jobArea": {
          "id": null,
          "description": null,
          "jobAreaRef": null
        },
        "dispatchOrder": {
          "dispatchOrderRef": null,
          "id": null,
          "originatorRecordId": null,
          "description": null,
          "totalOrdered": null,
          "totalTicketed": {
            "reasonCode": null,
            "loadNumber": null,
            "value": null,
            "uomCode": null
          }
        },
        "order": {
          "orderRef": null,
          "id": null
        },
        "orderRequest": {
          "orderRequestRef": null,
          "id": null,
          "description": null
        },
        "orderDiscipline": {
          "disciplineRef": null,
          "id": null,
          "description": null
        },
        "invoiceFrequency": null,
        "commerceTypeCode": "SELL",
        "salesAnalysis": {
          "id": null,
          "description": null
        },
        "deliveryOption": null,
        "transportationTermsCode": null,
        "inventoryLocation": null,
        "supplierSalesOrder": {
          "id": null,
          "originatorRecordId": null,
          "name": null
        },
        "origin": {
          "originatorRecordId": "BES4",
          "id": "BES4",
          "description": "IB Brussel",
          "latitude": 50.866695,
          "longitude": 4.35333,
          "address": {
            "address1": "Havenlaan 63 Avenue du port",
            "city": "Brussel",
            "postalCode": "1000 BE",
            "countryCode": "BEN",
            "contact": {}
          },
          "scale": {
            "id": null,
            "name": null
          }
        },
        "invoice": {
          "id": null,
          "invoiceDateTime": null
        },
        "destination": {
          "deliveryZone": {
            "id": null,
            "description": null
          },
          "deliveryBlock": null,
          "deliveryLots": [],
          "description": "undefined undefined",
          "address": {
            "contact": {}
          }
        },
        "primaryItemType": "CONCRETE",
        "lineItems": [
          {
            "customerOrderItem": {
              "haulCostCode": null,
              "productCostCode": null,
              "purchaseOrder": {
                "number": null,
                "line": null
              },
              "typeOfWork": null
            },
            "item": {
              "productRef": null,
              "itemType": "CONCRETE",
              "itemSubType": null,
              "pricingPlanId": null,
              "itemCategoryId": null
            },
            "customerItem": {
              "id": null,
              "description": null
            },
            "quantity": {
              "uomCode": "M3"
            },
            "soldQuantity": {
              "value": null,
              "uomCode": null
            },
            "unitPrice": 0,
            "extendedPrice": 0,
            "orderedQuantity": null,
            "batchQuantity": {
              "value": null,
              "uomCode": null
            },
            "returnedQuantity": {
              "value": null,
              "uomCode": null
            },
            "dumpedQuantity": {
              "value": null,
              "uomCode": null
            },
            "reshippedTicket": {
              "id": null,
              "originatorRecordId": null,
              "originatorSystemType": null,
              "originatorSystemId": null,
              "reshippedQuantity": {
                "value": null,
                "uomCode": null
              }
            },
            "isPrimary": true
          }
        ],
        "priceSummary": {
          "taxRate": null,
          "pricingLocation": null,
          "subtotalPrice": 0,
          "taxRegion": null,
          "taxCode": null,
          "taxAmount": 0,
          "totalPrice": 0,
          "totalFreightCharge": 0,
          "subTotalPrice": 0
        },
        "eTicketStatus": null,
        "customerStatus": {
          "statusCode": "PENDING"
        },
        "eTicketContacts": [],
        "supplierStatus": {
          "reasonCode": null,
          "statusCode": "CREATED"
        },
        "displayLabel": "#PH_TICKET_NUMBER#",
        "sk": {
          "dispatchDay": "27",
          "dispatchMonth": "06",
          "dispatchYear": "2022",
          "dispatchHour": "19",
          "carrierRef": null,
          "id": "#PH_TICKET_NUMBER#",
          "sourceTenantRef": "#PH_EID#",
          "uom": "M3",
          "lookupKey": "#PH_TICKET_NUMBER#",
          "tenantRef": "#PH_EID#",
          "carrierId": null,
          "carrierName": null,
          "vehicleId": "#PH_VEHICLE_NUMBER#",
          "vehicleDescription": "#PH_VEHICLE_NUMBER#",
          "customerJob": null,
          "disciplineId": null,
          "customerName": null,
          "orderTicketRef": null,
          "customerRef": null,
          "testResultStatus": null,
          "supplierStatus": "CREATED",
          "projectName": null,
          "projectId": null,
          "haulUnitCost": 0,
          "supplierId": null,
          "dispatchOrderDescription": null,
          "dispatchDate": "#PH_DATE_NOW#",
          "productUnitCost": 0,
          "sourceSystem": "HC",
          "dispatchOrderId": null,
          "jobAreaId": null,
          "jobAreaDescription": null,
          "customerStatus": "PENDING",
          "originId": "BES4",
          "customerId": null,
          "supplierVoided": false,
          "crn": "22461204-c68e-5331-80dc-90f6c3765e84",
          "productType": "CONCRETE",
          "latestEventStatus": "CREATED",
          "ticketEventStatus": "CREATED",
          "ticketEventStatusTime": "#PH_DATE_NOW#",
          "supplierName": null,
          "quantity": null,
          "ticketType": "production",
          "disciplineDescription": null,
          "supplierRef": "#PH_EID#",
          "purchaseOrder": null,
          "invoiceId": null,
          "originDescription": "IB Brussel"
        }
      }
    }
  },
  "PMP": {}
};

const CANCEL_JSON = {
  "Item": {
    "data": {
      "typeId": "ticket",
      "CONNEX": {
        "EID": "#PH_EID#"
      },
      "id": "#PH_TICKET_NUMBER#",
      "sk": {
        "supplierVoided": true
      },
      "customData": {
        "voidReasonCode": 1,
        "voidReasonRemark": "Cancellation before Loading"
      }
    }
  }
};
