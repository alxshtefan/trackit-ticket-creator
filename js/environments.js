/**
 *
 * If you need to add one more environment, please follow format below.
 *
 *  <name_to_display> : <eid>
 *
 */

const ENVIRONMENTS = {
  "k8-rogueone": "RogueOne-5678",
  "k8-roguetwo": "basegps7"
};