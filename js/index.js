document.addEventListener("DOMContentLoaded", function () {
  fillSelect(VEHICLE_NUMBER_SELECT, VEHICLE_NUMBERS, "Select vehicle");
  fillSelect(ENVIRONMENT_SELECT, ENVIRONMENTS, "Select environment");
});

function fillSelect(selectObject, keyValueMap, hintText) {
  let defaultOption = document.createElement("option");
  defaultOption.text = hintText;
  defaultOption.disabled = true;
  defaultOption.selected = true;
  selectObject.append(defaultOption);

  for (let vehicleNumberName in keyValueMap) {
    let option = document.createElement("option");
    option.text = vehicleNumberName;
    option.title = vehicleNumberName;
    option.value = keyValueMap[vehicleNumberName];
    selectObject.append(option);
  }
}

function generateTicketJson() {
  let vehicleNumber = VEHICLE_NUMBER_SELECT.value;
  let environment = ENVIRONMENT_SELECT.value;
  let ticketType = TICKET_TYPE_SELECT.value;
  if (ticketType.length === 0) {
    _toggleError(TICKET_TYPE_SELECT, true);
    return;
  } else {
    _toggleError(TICKET_TYPE_SELECT, false);
  }

  let ticketNumber = _getTicketNumber(ticketType);
  let dateNow = new Date().toISOString().replace(/.\d+Z/, 'Z');

  CREATE_TICKET_AREA.value = JSON.stringify(CREATE[ticketType], null, 2)
    .replaceAll(VEHICLE_NUMBER_PLACEHOLDER, vehicleNumber)
    .replaceAll(TICKET_NUMBER_PLACEHOLDER, ticketNumber)
    .replaceAll(ENVIRONMENT_PLACEHOLDER, environment)
    .replaceAll(DATE_NOW_PLACEHOLDER, dateNow);

  CANCEL_TICKET_AREA.value = JSON.stringify(CANCEL_JSON, null, 2)
    .replaceAll(VEHICLE_NUMBER_PLACEHOLDER, vehicleNumber)
    .replaceAll(TICKET_NUMBER_PLACEHOLDER, ticketNumber)
    .replaceAll(ENVIRONMENT_PLACEHOLDER, environment)
    .replaceAll(DATE_NOW_PLACEHOLDER, dateNow);
}

function _getTicketNumber(ticketType) {
  let ticketNumber = TICKET_NUMBER_INPUT.value;
  if (ticketNumber.length === 0) {
    ticketNumber = ticketType + '.'
      + new Date().getTime().toString().slice(-7, 15) + '.'
      + Math.floor(Math.random() * 10000000000) + '_'
      + new Date().getTime().toString().slice(-3, 15);
  }
  return ticketNumber;
}

function _toggleError(component, turnOn) {
  if (turnOn) {
    component.classList.add("error");
    component.parentElement.parentElement.classList.add("error-color");
  } else {
    component.classList.remove("error");
    component.parentElement.parentElement.classList.remove("error-color");
  }
}

function copyToClipboard(action) {
  let json;
  if (action === 'create') {
    json = CREATE_TICKET_AREA.value;
  }
  if (action === 'cancel') {
    json = CANCEL_TICKET_AREA.value;
  }
  navigator.clipboard.writeText(json)
    .then(() => console.log('Successfully copied to the clipboard'));
}
